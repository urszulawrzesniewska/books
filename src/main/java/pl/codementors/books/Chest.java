package pl.codementors.books;

public class Chest {
    private Shelf[] shelves = new Shelf[10];

    public void setShelf(int index, Shelf shelf) {
        shelves[index] = shelf;
    }

    public Shelf getShelf(int index) {

        return shelves[index];
    }

}