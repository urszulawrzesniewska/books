package pl.codementors.books;

import java.util.Scanner;


public class Main {


    public static void main(String[] args) {
        Shelf newShelf[] = new Shelf[10];

        Scanner inputScanner = new Scanner(System.in);


        boolean running = true;

        System.out.println("Books catalogue");


        while (running) {

            int command = inputScanner.nextInt();

            switch (command) {
                case 1: {
                    System.out.println("title:");
                    String title = inputScanner.next();
                    System.out.println("author:");
                    String author = inputScanner.next();
                    System.out.println("year");
                    int year = inputScanner.nextInt();
                    System.out.println("table");
                    int table = inputScanner.nextInt();
                    System.out.println("shelf number");
                    int Shelfnumber = inputScanner.nextInt();
                    Book book = new Book();

                    book.setAuthor(author);
                    book.setName(title);
                    book.setYear(year);
                    if (newShelf[Shelfnumber] == null) {
                        newShelf[Shelfnumber] = new Shelf();
                    }
                    newShelf[Shelfnumber].setBook(table, book);


                    break;
                }
                case 2: {
                    Book book;
                    System.out.println("table");
                    int table = inputScanner.nextInt();
                    int Shelfnumber = inputScanner.nextInt();
                    book = newShelf[Shelfnumber].getBook(table);
                    System.out.println(book.getName());
                    System.out.println(book.getAuthor());
                    System.out.println(book.getYear());

                    break;
                }
                case 3: {
                    System.out.println("list all books");
                    for (Shelf shelf : newShelf) {
                        if(shelf!=null) {
                            for (int i = 0; i < 20; i++) {
                                Book book;
                                book = shelf.getBook(i);
                                if (book != null) {
                                    System.out.println(book.getName());
                                    System.out.println(book.getAuthor());
                                    System.out.println(book.getYear());
                                }
                            }
                        }
                    }
                    break;
                }

                case 0: {
                    running = false;
                    break;
                }
                default: {
                    System.out.println("Valid commands are:\n" +
                            "1 - add,\n" +
                            "2 - print,\n" +
                            "0 - quit.\n");
                }
            }
        }
    }
}