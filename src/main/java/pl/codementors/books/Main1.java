package pl.codementors.books;

import java.util.Scanner;

public class Main1 {

        public static void main(String[] args) {

            //1) utworzenie Scannera

            Scanner inputScanner = new Scanner(System.in);

            Book[] books = new Book[10];

            //2) Utworzenie obiektu Book
            Book book = new Book();

            System.out.println("Podaj Tytul");

            //3) pobranie tytulu

            String name = inputScanner.nextLine();

            //4) ustawienie pobranego tytulu ksiazce

            book.setName(name);

            System.out.println(book.getName());

            //5) Podaj autora

            System.out.println("Podaj autora");

            //6) Ustaw ksiazce podanego autora

            String author = inputScanner.nextLine();

            book.setAuthor(author);

            System.out.println(book.getAuthor());

            //7) Wyswietl "Give year"

            System.out.println("Podaj rok");

            //8) Ustaw rok ksiazce (hint uzyj nextInt)

            int year = inputScanner.nextInt();
            book.setYear(year);
            System.out.println(book.getYear());
        }
}
