package pl.codementors.books;

public class Shelf {
    private Book[] books = new Book[20];
    public void setBook(int index, Book book) {
        books[index] = book;
    }

    public Book getBook(int index) {

        return books[index];
    }

}